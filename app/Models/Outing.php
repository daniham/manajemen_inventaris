<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outing extends Model
{
    protected $table = 'outing';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_barang', 'date_of_out','outstock_stock','penanggungjawab',
    ];

}
