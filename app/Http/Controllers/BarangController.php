<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Barang;

class BarangController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $barang = Barang::all();
        return response()->json($barang);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:barangs",
            "type" => "required",
            "price" => "required",
            "purchase_date" => "required",
            "stock" => "required",
            "status" => "required"
        ]);
        $data = $request->all();
        $barang = Barang::create($data);
        return response()->json($barang);
    }

    public function show($id)
    {
        $barang = Barang::find($id);
        return response()->json($barang);
    }

    public function update(Request $request, $id)
    {
        $barang = Barang::find($id);
        
        if (!$barang) {
            return response()->json(['message' => 'Data not found'], 404);
        }
        
        $this->validate($request, [
            "name" => "required|unique:barangs",
            "type" => "required",
            "price" => "required",
            "purchase_date" => "required",
            "stock" => "required",
            "status" => "required"
        ]);

        $data = $request->all();
        $barang->fill($data);
        $barang->save();

        return response()->json($barang);
    }

    public function destroy($id)
    {
        $barang = Barang::find($id);
        
        if (!$barang) {
            return response()->json(['message' => 'Data not found'], 404);
        }

        $barang->delete();

        return response()->json(['message' => 'Data deleted successfully'], 200);
    }
}
