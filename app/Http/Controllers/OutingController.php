<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Outing;

class OutingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $outing = Outing::all();
        return response()->json($outing);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            "id_barang" => "required",
            "date_of_out" => "required",
            "outstock_stock" => "required",
            "penanggungjawab" => "required"
        ]);
        $data = $request->all();
        $outing = Outing::create($data);
        return response()->json($outing);
    }

    public function show($id)
    {
        $outing = Outing::find($id);
        return response()->json($outing);
    }

    public function update(Request $request, $id)
    {
        $outing = Outing::find($id);
        
        if (!$outing) {
            return response()->json(['message' => 'Data not found'], 404);
        }
        
        $this->validate($request, [
            "id_barang" => "required",
            "date_of_out" => "required",
            "outstock_stock" => "required",
            "penanggungjawab" => "required"
        ]);

        $data = $request->all();
        $outing->fill($data);
        $outing->save();

        return response()->json($outing);
    }

    public function destroy($id)
    {
        $outing = Outing::find($id);
        
        if (!$outing) {
            return response()->json(['message' => 'Data not found'], 404);
        }

        $outing->delete();

        return response()->json(['message' => 'Data deleted successfully'], 200);
    }
}
