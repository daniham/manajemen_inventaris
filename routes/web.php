<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register','UserController@register');
$router->post('/login','UserController@login');


$router->post('/barang','BarangController@create');
$router->get('/barang','BarangController@index');
$router->get('/barang/{id}','BarangController@show');
$router->put('/barang/{id}','BarangController@update');
$router->delete('/barang/{id}','BarangController@destroy');


$router->post('/incoming','IncomingController@create');
$router->get('/incoming','IncomingController@index');
$router->get('/incoming/{id}','IncomingController@show');
$router->put('/incoming/{id}','IncomingController@update');
$router->delete('/incoming/{id}','IncomingController@destroy');

$router->post('/outing','OutingController@create');
$router->get('/outing','OutingController@index');
$router->get('/outing/{id}','OutingController@show');
$router->put('/outing/{id}','OutingController@update');
$router->delete('/outing/{id}','OutingController@destroy');