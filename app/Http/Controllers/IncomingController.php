<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Incoming;

class IncomingController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $incoming = Incoming::all();
        return response()->json($incoming);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            "id_barang" => "required",
            "date_of_receipt" => "required",
            "receipt_stock" => "required",
            "penerima" => "required"
        ]);
        $data = $request->all();
        $incoming = Incoming::create($data);
        return response()->json($incoming);
    }

    public function show($id)
    {
        $incoming = Incoming::find($id);
        return response()->json($incoming);
    }

    public function update(Request $request, $id)
    {
        $incoming = Incoming::find($id);
        
        if (!$incoming) {
            return response()->json(['message' => 'Data not found'], 404);
        }
        
        $this->validate($request, [
            "id_barang" => "required",
            "date_of_receipt" => "required",
            "receipt_stock" => "required",
            "penerima" => "required"
        ]);

        $data = $request->all();
        $incoming->fill($data);
        $incoming->save();

        return response()->json($incoming);
    }

    public function destroy($id)
    {
        $incoming = Incoming::find($id);
        
        if (!$incoming) {
            return response()->json(['message' => 'Data not found'], 404);
        }

        $incoming->delete();

        return response()->json(['message' => 'Data deleted successfully'], 200);
    }
}
